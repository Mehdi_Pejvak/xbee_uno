/*
*  @name XBEE_2
*  @info The program belongs to the first XBee module, set as a router. It includes three LEDs, 
*        WorkingLED: while module is working
*        StatusLED: shows the ststus of Transmission/Recieve
*        ErrorLED: while error happens
*        and a push bottom to send a command to router 2.
*        
*        Coor = 0013A20040C8E51F       
*        R001  = 0013A20040C2651F
*        R002  = 0013A20040C2653C
*        
*/
/*    Include     */
#include <XBee.h>
#include <XBeeGenDef.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Thread.h>
#include <map>



/*    Definition     */
// Data wire is plugged into port PB1 on the Board
#define ONE_WIRE_BUS 33 //PA12
#define TEMPERATURE_PRECISION 9 // Lower resolution

#define  WorkingLED  PC13
#define  PushBottom  PA11
#define  StatusLED   PA15
#define  ErrorLED    PA12

uint8_t payloadRX[6] = {0};
uint8_t* routeRecordData;

typedef std::map <String,String> remoteXbee64;
typedef std::map <String,String> remoteXbee16;
typedef std::map <String,String> sourceRoute;

remoteXbee64["Cor"] = ["0013A20040C8E51F"];
remoteXbee64["R001"] = ["0013A20040C2651F"];


int count;
float temperature = 0;


/* Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)  */
OneWire oneWire(ONE_WIRE_BUS);

/* Pass our oneWire reference to Dallas Temperature.  */
DallasTemperature sensors(&oneWire);

/*    Functions     */
void xBeeSendCallback();
void sourceRoutingCallBack();
void flashLed(int pin, int times, int wait);
int handleMsg();

/* union to convery float to byte string  */
union u_tag {
  uint8_t b[2];
  float fval;
} function;

union u_tag1 {
  uint8_t b[4];
  float fval;
} msg;

/*    Initiation of Classes 
      Instantiate Xbee Class and Set Other Configurations  */
XBee xbee = XBee();

XBeeAddress64 addCoord = XBeeAddress64(0X0013A200,0X40C8E51F);
XBeeAddress64 addR001 = XBeeAddress64(0X0013A200,0X40C2651F);

ZBTxRequest zBTxReq = ZBTxRequest(addCoord, payloadRX, sizeof(payloadRX));;
ZBTxStatusResponse txStatus = ZBTxStatusResponse();
XBeeResponse response = XBeeResponse();
ZBRxResponse zBRxRes = ZBRxResponse();
ZBRxResponse zBRouteRecInd = ZBRxResponse();

/* Instantiate Thread Class  */
Thread xBeeSendThread = Thread();
Thread XBeeRouteRecIndThread = Thread();


void setup() 
{
    // Set up pins
    pinMode(PushBottom, INPUT);
    pinMode(WorkingLED,OUTPUT);
    pinMode(StatusLED,OUTPUT);
    pinMode(ErrorLED,OUTPUT);
    digitalWrite(StatusLED,LOW);
    
    // Set up Serial ports
    Serial1.begin(9600);
    xbee.begin(Serial1);
    
    xBeeSendThread.onRun(xBeeSendCallback);
    xBeeSendThread.setInterval(1000);

    XBeeRouteRecIndThread.onRun(sourceRoutingCallBack);
    XBeeRouteRecIndThread.setInterval(1000);

    // Start up the library
    sensors.begin();
    
    flashLed(WorkingLED,3,300);
}


void loop() 
{

/*
  if (digitalRead(PushBottom) == HIGH)
  {
    payloadRX[0] = {'0'};
    payloadRX[1] = {'1'};
    zBTxReq.setPayload(payloadRX);
    zBTxReq.setPayloadLength(sizeof(payloadRX));
    zBTxReq.setAddress64(addRout1);
    xbee.send(zBTxReq);
    //flashLed(WorkingLED,1,100);
    
    if(xbee.readPacket(1000))
    {
      if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE)
      {
        xbee.getResponse().getZBTxStatusResponse(txStatus);
        if (txStatus.getDeliveryStatus() == SUCCESS)
        {
          flashLed(StatusLED,5,50);
        } else
        {
          flashLed(ErrorLED,3,500);
        }
      }
    } else if (xbee.getResponse().isError())
    {
      flashLed(ErrorLED, 2, 500);
    } else
    {
      flashLed(ErrorLED, 1, 500);
    }
  }
  */
  
  xbee.readPacket();
  if(xbee.getResponse().isAvailable())
  {
    flashLed(WorkingLED,2,50);

    if (xbee.getResponse().getApiId() == ROUTE_RECORD_INDICATOR)
    {
      xbee.getResponse().getZBRxResponse(zBRouteRecInd);
      if (XBeeRouteRecIndThread.shouldRun()){
          XBeeRouteRecIndThread.run();
        }
    }
    if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE)
    {
      flashLed(WorkingLED,3,100);
      xbee.getResponse().getZBRxResponse(zBRxRes);
      for (int i = 0; i < zBRxRes.getDataLength(); i++) 
      {
        payloadRX[i] = zBRxRes.getData()[i];
      }
      
      if (handleMsg() == FLASH_LED)
      {
        flashLed(WorkingLED,5,1000);
      }
      
      if (handleMsg() == READ_PIN)
      {
        payloadRX[0] = {'0'};
        payloadRX[1] = {'2'};
        if (digitalRead(PushBottom) == HIGH)
        {
          payloadRX[2] = {'H'};
          payloadRX[3] = {'I'};
          payloadRX[4] = {'G'};
          payloadRX[5] = {'H'};
        } else if (digitalRead(PushBottom) == LOW)
        {
          payloadRX[2] = {'L'};
          payloadRX[3] = {'O'};
          payloadRX[4] = {'W'};
        }
        zBTxReq.setPayload(payloadRX);
        zBTxReq.setPayloadLength(sizeof(payloadRX));
        zBTxReq.setAddress64(zBRxRes.getRemoteAddress64()); 
        xbee.send(zBTxReq);
        
        if(xbee.readPacket(1000))
        {
          if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE)
          {
            xbee.getResponse().getZBTxStatusResponse(txStatus);
            if (txStatus.getDeliveryStatus() == SUCCESS)
            {
              flashLed(StatusLED,5,50);
            } else
            {
              flashLed(ErrorLED,3,500);
            }
          }
        } else if (xbee.getResponse().isError())
        {
          flashLed(ErrorLED, 2, 500);
        } else
        {
          flashLed(ErrorLED, 1, 500);
        }
      }
      
      if (handleMsg() == READ_TEMPERATURE)
      {
        flashLed(WorkingLED, 2, 200);
        if (xBeeSendThread.shouldRun()){
          xBeeSendThread.run();
        }

        if(xbee.readPacket(1000))
        {
          if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE)
          {
            xbee.getResponse().getZBTxStatusResponse(txStatus);
            if (txStatus.getDeliveryStatus() == SUCCESS)
            {
              flashLed(StatusLED,5,50);
            } else
            {
              flashLed(ErrorLED,3,500);
            }
          }
        } else if (xbee.getResponse().isError())
          {
          flashLed(ErrorLED, 2, 500);
        } else
        {
          flashLed(ErrorLED, 1, 500);
        }
      }
    } else 
    {
      // not something we were expecting
      flashLed(ErrorLED, 1, 25);    
    }
  }
  delay(1000);
}


/*  Callback for xBeeSendThread  */
void xBeeSendCallback()
{
  sensors.requestTemperatures();
  temperature = sensors.getTempCByIndex(0);
  
  payloadRX[0]='0';
  payloadRX[1]='3';

  /* Check to see if temperature is NaN */
  if (!isnan(temperature)){
    msg.fval = temperature;
    for (int i=0;i<4;i++)
     {
        payloadRX[i+2]=msg.b[i];
     }
  }
  
  zBTxReq.setPayloadLength(sizeof(payloadRX));
  zBTxReq.setPayload(payloadRX);
  zBTxReq.setAddress64(zBRxRes.getRemoteAddress64());

  xbee.send(zBTxReq);
  
  delay(500);
  flashLed(WorkingLED,5,100);
}

/* Callback for sourceRoutingThread */
void sourceRoutingCallBack()
{
   routeRecordData = xbee.getResponse().getFrameData();
   
   
}

/* Function for handling the message */

int handleMsg()
{
  if (payloadRX[0] == '0' && payloadRX[1] == '1')
  {
    return FLASH_LED;
  } else if (payloadRX[0] == '0' && payloadRX[1] == '2')
  {
    return READ_PIN;
  } else if (payloadRX[0] == '0' && payloadRX[1] == '3')
  {
    return READ_TEMPERATURE;
  } else if (payloadRX[0] == '0' && payloadRX[1] == '4')
  {
    return READ_HUMIDITY;
  } else if (payloadRX[0] == '0' && payloadRX[1] == '5')
  {
    return WRITE_PIN;
  }
}

/* Function for flashing LED */
void flashLed(int pin, int times, int wait) 
{
  for (int i = 0; i < times; i++) {
    digitalWrite(pin, HIGH);
    delay(wait);
    digitalWrite(pin, LOW);
    delay(wait);
  }
}
