
/*
 *  @name XBEE_1
 *  @info The program belongs to the first XBee module, set as a router. It includes three LEDs, 
 *        WorkingLED: while module is working
 *        StatusLED: shows the ststus of Transmission/Recieve
 *        ErrorLED: while error happens
 *        and a push bottom to send a command to router 2.
 *        and flash by the command from other modules.
 *        
 *        Coor = 0013A20040C2651F       
 *        R001  = 0013A20040C8E51F
 *        R002  = 0013A20040C2653C
 */

/*    Include     */
#include <XBee.h>
#include <XBeeGenDef.h>
#include <Thread.h>

/*    Definition     */
#define WorkingLED  PC13
#define PushBottom  PA11
#define StatusLED   PA15
#define ErrorLED    PA12
#define PinSts      PB3

uint8_t payloadRX[6] = {0};

int count;

/*    Functions     */
void xBeeSendCallback();
void sourceRoutingCallBack();
void flashLed(int pin, int times, int wait);
int handleMsg();

/*  Initiation of Classes 
    Instantiate Xbee Class and Set Other Configurations  */
XBee xbee = XBee();

XBeeAddress64 addCoord = XBeeAddress64(0X0013A200,0X40C2651F);
XBeeAddress64 addR002 = XBeeAddress64(0X0013A200,0X40C2653C);

ZBTxRequest zBTxReq = ZBTxRequest(addCoord, payloadRX, sizeof(payloadRX));
ZBTxStatusResponse txStatus = ZBTxStatusResponse();
XBeeResponse response = XBeeResponse();
ZBRxResponse zBRxRes = ZBRxResponse();
ZBRxResponse zBRouteRecInd = ZBRxResponse();

/* Instantiate Thread Class  */
Thread xBeeSendThread = Thread();
Thread XBeeRouteRecIndThread = Thread();

void setup() 
{
  /* Set up pins  */
  pinMode(PushBottom, INPUT);
  pinMode(WorkingLED,OUTPUT);
  pinMode(StatusLED,OUTPUT);
  pinMode(ErrorLED,OUTPUT);
  pinMode(PinSts,INPUT);

  /* Set up Serial ports  */
  Serial1.begin(9600);
  xbee.begin(Serial1);

  xBeeSendThread.onRun(xBeeSendCallback);
  xBeeSendThread.setInterval(1000);

  XBeeRouteRecIndThread.onRun(sourceRoutingCallBack);
  XBeeRouteRecIndThread.setInterval(1000);
  
  flashLed(WorkingLED,3,300);
}

void loop() 
{
 /* if (digitalRead(PushBottom) == HIGH)
  {
    payloadRX[0] = {'0'};
    payloadRX[1] = {'1'};
    zBTxReq.setPayload(payloadRX);
    zBTxReq.setPayloadLength(sizeof(payloadRX));
    zBTxReq.setAddress64(addrRout1);
    xbee.send(zBTxReq);
    flashLed(WorkingLED,1,100);

    if(xbee.readPacket(1000))
    {
      if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE)
      {
        xbee.getResponse().getZBTxStatusResponse(txStatus);
        if (txStatus.getDeliveryStatus() == SUCCESS)
        {
          flashLed(StatusLED,5,50);
        } else
        {
          flashLed(ErrorLED,3,500);
        }
      }
    } else if (xbee.getResponse().isError())
    {
      flashLed(ErrorLED, 2, 500);
    } else
    {
      flashLed(ErrorLED, 1, 500);
    }
  }*/

  xbee.readPacket();
  if(xbee.getResponse().isAvailable())
  {
    flashLed(WorkingLED,2,50);
    if (xbee.getResponse().getApiId() == ROUTE_RECORD_INDICATOR)
    {
      xbee.getResponse().getZBRxResponse(zBRouteRecInd);
      if (XBeeRouteRecIndThread.shouldRun()){
          XBeeRouteRecIndThread.run();
        }
    }
    
    if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE)
    {
      xbee.getResponse().getZBRxResponse(zBRxRes);
      
      for (int i = 0; i < zBRxRes.getDataLength(); i++) 
      {
          payloadRX[i] = zBRxRes.getData()[i];
      }


      if (handleMsg() == FLASH_LED)
      {
        flashLed(WorkingLED,5,1000);
      }

       if (handleMsg() == READ_PIN)
      {
        payloadRX[0] = {'0'};
        payloadRX[1] = {'2'};
        if (digitalRead(PushBottom) == HIGH)
        {
          payloadRX[2] = {'H'};
          payloadRX[3] = {'I'};
          payloadRX[4] = {'G'};
          payloadRX[5] = {'H'};
        } else if (digitalRead(PushBottom) == LOW)
        {
          payloadRX[2] = {'L'};
          payloadRX[3] = {'O'};
          payloadRX[4] = {'W'};
        }
        zBTxReq.setPayload(payloadRX);
        zBTxReq.setPayloadLength(sizeof(payloadRX));
        zBTxReq.setAddress64(zBRxRes.getRemoteAddress64());
        xbee.send(zBTxReq);
        flashLed(WorkingLED,1,100);

        if(xbee.readPacket(1000))
        {
          if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE)
          {
            xbee.getResponse().getZBTxStatusResponse(txStatus);
            if (txStatus.getDeliveryStatus() == SUCCESS)
            {
              flashLed(StatusLED,5,50);
            } else
            {
              flashLed(ErrorLED,3,500);
            }
          }
        } else if (xbee.getResponse().isError())
        {
          flashLed(ErrorLED, 2, 500);
        } else
        {
          flashLed(ErrorLED, 1, 500);
        }
      }
    } else 
    {
       // not something we were expecting
       flashLed(ErrorLED, 1, 25);    
    }
  }

}

/*  Callback for xBeeSendThread  */
void xBeeSendCallback()
{
  int i=0;
}
/*  Callback for sourceRoutingThread  */
void sourceRoutingCallBack()
{
  int i=0;
}

/*  Function for handling the message  */
int handleMsg()
{
  if (payloadRX[0] == '0' && payloadRX[1] == '1')
  {
    return FLASH_LED;
  } else if (payloadRX[0] == '0' && payloadRX[1] == '2')
  {
    return READ_PIN;
  } else if (payloadRX[0] == '0' && payloadRX[1] == '3')
  {
    return READ_TEMPERATURE;
  } else if (payloadRX[0] == '0' && payloadRX[1] == '4')
  {
    return READ_HUMIDITY;
  } else if (payloadRX[0] == '0' && payloadRX[1] == '5')
  {
    return WRITE_PIN;
  }
}

/*  Function for flashing LED  */
void flashLed(int pin, int times, int wait) 
{

  for (int i = 0; i < times; i++) {
    digitalWrite(pin, HIGH);
    delay(wait);
    digitalWrite(pin, LOW);

    if (i + 1 < times) {
      delay(wait);
    }
  }
}
